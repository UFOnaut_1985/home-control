import 'package:flutter/material.dart';
import 'package:home_control/network/network_helper.dart';
import 'package:home_control/widget/cell_widget.dart';
import 'package:home_control/widget/home_bottom_sheet.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  static const ID = "home_screen";

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Consumer<NetworkHelper>(
          builder: (context, networkHelper, child) => Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: RefreshIndicator(
                    onRefresh: () => networkHelper.fetchSensorsData(),
                    child: SingleChildScrollView(
                      physics: const AlwaysScrollableScrollPhysics(),
                      child: GridView.count(
                        shrinkWrap: true,
                        primary: false,
                        padding: const EdgeInsets.all(20),
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        crossAxisCount: 2,
                        children: <Widget>[
                          CellWidget(
                              iconData: Icons.home,
                              bigText:
                                  "${networkHelper.getInsideAirBeforeExchange()}°",
                              firstDescription: "Home temperature",
                              backgroundColor: Colors.deepOrangeAccent),
                          CellWidget(
                            iconData: Icons.wb_sunny,
                            bigText:
                                "${networkHelper.getOutsideAirBeforeExchange()}°",
                            firstDescription: "Outside temperature",
                            backgroundColor: Colors.lightBlue,
                          ),
                          CellWidget(
                              iconData: Icons.local_florist,
                              bigText:
                                  "${networkHelper.getOutsideAirAfterExchange()}°",
                              firstDescription: "Recuperated air temperature",
                              backgroundColor: Colors.green),
                          CellWidget(
                              iconData: Icons.autorenew,
                              bigText:
                                  "${networkHelper.getRecuperationPercentage()}%",
                              firstDescription: "Heat exchange",
                              backgroundColor: Colors.lightGreen,
                              animateIcon: networkHelper.isVentilationActive()),
                          CellWidget(
                              onPressed: () => showModalBottomSheet(
                                  context: context,
                                  builder: (context) =>
                                      HomeBottomSheet.temperature(
                                          networkHelper)),
                              iconData: Icons.whatshot,
                              bigText: "${networkHelper.getUserTemperature()}°",
                              firstDescription:
                                  "Heating is ${networkHelper.isHeatActive() ? "on" : "off"}",
                              backgroundColor: networkHelper.isHeatActive()
                                  ? Colors.red
                                  : Colors.grey,
                              animateIcon: networkHelper.isHeatActive()),
                          CellWidget(
                              onPressed: () => showModalBottomSheet(
                                  context: context,
                                  builder: (context) =>
                                      HomeBottomSheet.ventilation(
                                          networkHelper)),
                              iconData: Icons.toys,
                              bigText: "${networkHelper.getUserVentilation()}%",
                              firstDescription: "Ventilation power",
                              backgroundColor: Colors.teal,
                              animateIcon: networkHelper.isVentilationActive()),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
