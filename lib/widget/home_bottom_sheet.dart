import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_control/network/network_helper.dart';

class HomeBottomSheet extends StatefulWidget {
  String title;
  String description;
  int min;
  int max;
  int initialValue;
  Color color;
  NetworkHelper networkHelper;
  bool isTemperatureSelector = true;

  HomeBottomSheet(
      {@required this.title,
      @required this.min,
      @required this.max,
      this.initialValue});

  HomeBottomSheet.temperature(NetworkHelper networkHelper) {
    this.networkHelper = networkHelper;
    this.title = "Select target temperature";
    this.min = 20;
    this.max = 25;

    this.initialValue = networkHelper.getUserTemperature();
    this.color = networkHelper.isHeatActive() ? Colors.red : Colors.grey;
    this.isTemperatureSelector = true;
  }

  HomeBottomSheet.ventilation(NetworkHelper networkHelper) {
    this.networkHelper = networkHelper;
    this.title = "Select ventilation power";
    this.min = 0;
    this.max = 100;
    this.initialValue = networkHelper.getUserVentilation();
    this.color = networkHelper.isHeatActive() ? Colors.red : Colors.grey;
    this.isTemperatureSelector = false;
  }

  @override
  _HomeBottomSheetState createState() => _HomeBottomSheetState(
      min, max, initialValue, networkHelper, isTemperatureSelector);
}

class _HomeBottomSheetState extends State<HomeBottomSheet> {
  int min;
  int max;
  int initialValue;
  NetworkHelper networkHelper;
  bool isTemperatureSelector;

  _HomeBottomSheetState(this.min, this.max, this.initialValue,
      this.networkHelper, this.isTemperatureSelector);

  @override
  Widget build(BuildContext context) {
    String text = isTemperatureSelector
        ? "Target temperature is ${initialValue.toString()}°C"
        : "Ventilation is ${initialValue.toString()}%";
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          )
        ],
      ),
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0, top: 10),
                  child: Text(
                    text,
                    style: TextStyle(fontSize: 30, color: Colors.black54),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 5,
                  child: Slider(
                    min: min.toDouble(),
                    max: max.toDouble(),
                    onChanged: (double newValue) {
                      setState(() {
                        initialValue = newValue.round();
                      });
                    },
                    value: initialValue.toDouble(),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 10, right: 10),
                  alignment: Alignment.centerRight,
                  child: RaisedButton(
                    elevation: 8,
                    onPressed:
                        initialValue != networkHelper.getUserTemperature()
                            ? onSavePressed
                            : null,
                    child: Text(
                      "Save",
                      style: TextStyle(fontSize: 20, color: Colors.black54),
                    ),
                  ),
                ),
              ])),
    );
  }

  void onSavePressed() {
    if (isTemperatureSelector)
      networkHelper.updateUserSettings(temperature: initialValue);
    else
      networkHelper.updateUserSettings(ventilation: initialValue);

    Navigator.pop(context);
  }
}
