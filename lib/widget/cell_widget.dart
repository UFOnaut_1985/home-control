import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CellWidget extends StatefulWidget {
  IconData iconData;
  String bigText = "";
  String firstDescription = "";
  String secondDescription = "";
  Color backgroundColor = Colors.transparent;
  bool animateIcon = false;
  Function onPressed;

  CellWidget({
    this.iconData,
    this.bigText,
    this.firstDescription = "",
    this.secondDescription = "",
    this.backgroundColor,
    this.animateIcon = false,
    this.onPressed,
  });

  @override
  _CellWidgetState createState() => _CellWidgetState(onPressed);
}

class _CellWidgetState extends State<CellWidget>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;
  Function onPressed;

  _CellWidgetState(this.onPressed);

  initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this, value: 1);
    animation = CurvedAnimation(parent: controller, curve: Curves.linear);

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        controller.forward();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    controller?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.animateIcon) {
      //this will start the animation
      controller.forward();
    }

    return Material(
      elevation: 10,
      borderRadius: BorderRadius.circular(10),
      color: widget.backgroundColor,
      child: FlatButton(
        onPressed: () => onPressed(),
        padding: EdgeInsets.all(8),
        child: Container(
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.topRight,
                child: FadeTransition(
                  opacity: animation,
                  child: Icon(
                    widget.iconData,
                    color: Colors.white.withAlpha(120),
                    size: 60.0,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    widget.bigText,
                    style: TextStyle(
                      fontSize: 50,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    widget.firstDescription,
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    widget.secondDescription,
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
