import 'package:flutter/material.dart';
import 'package:home_control/network/network_helper.dart';
import 'package:provider/provider.dart';

import 'screen/home_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => NetworkHelper(),
      child: MaterialApp(
        title: 'Home Control',
        theme: ThemeData.light(),
        home: HomeScreen(),
        routes: {
          HomeScreen.ID: (context) => HomeScreen(),
        },
      ),
    );
  }
}
