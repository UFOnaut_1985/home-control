class SensorsData {
  num outsideAirTemperature;
  num outsideAirTemperatureAfterHeatExchange;
  num insideAirTemperature;
  num userTemperature;
  var userVentilation;
  var isHeatRelayActive;

  SensorsData(
      {this.outsideAirTemperature,
      this.outsideAirTemperatureAfterHeatExchange,
      this.insideAirTemperature,
      this.userTemperature,
      this.userVentilation,
      this.isHeatRelayActive});

  factory SensorsData.fromJson(Map<dynamic, dynamic> json) {
    return SensorsData(
      outsideAirTemperature: json["outsideAirTemperature"],
      outsideAirTemperatureAfterHeatExchange:
          json["outsideAirTemperatureAfterHeatExchange"],
      insideAirTemperature: json["insideAirTemperature"],
      userTemperature: json["userTemperature"],
      userVentilation: json["userVentilation"],
      isHeatRelayActive: json["isHeatRelayActive"],
    );
  }
}
