import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'model/sensors.dart';

class NetworkHelper with ChangeNotifier {
  NetworkHelper() {
    fetchSensorsData();
  }

  SensorsData _sensorsData;

  Future<void> fetchSensorsData() async {
    try {
      final response = await http.get('http://192.168.88.100/sensors');
      if (response.statusCode == 200) {
        _sensorsData = SensorsData.fromJson(jsonDecode(response.body));
        notifyListeners();
      } else {
        print("error");
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateUserSettings({int temperature, int ventilation}) async {
    try {
      final Map body = Map<String, int>();
      if (temperature != null && temperature > 20 && temperature <= 25)
        body["temperature"] = temperature;
      else {
        //TODO temperature not ok error
      }
      if (ventilation != null && ventilation > 0 && ventilation <= 100)
        body["ventilation"] = ventilation;
      else {
        //TODO ventilation not ok error
      }

      final response = await http.post(
        'http://192.168.88.100/settings',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        _sensorsData = SensorsData.fromJson(jsonDecode(response.body));
        notifyListeners();
      } else {
        print("error");
      }
    } catch (e) {
      print(e);
    }
  }

  double getOutsideAirAfterExchange() {
    return roundToPrecision(
        _sensorsData?.outsideAirTemperatureAfterHeatExchange?.toDouble());
  }

  double getInsideAirBeforeExchange() {
    return roundToPrecision(_sensorsData?.insideAirTemperature?.toDouble());
  }

  double getOutsideAirBeforeExchange() {
    return roundToPrecision(_sensorsData?.outsideAirTemperature?.toDouble());
  }

  bool isHeatActive() {
    return _sensorsData?.isHeatRelayActive ?? false;
  }

  bool isVentilationActive() {
    return getUserVentilation() > 0;
  }

  int getUserTemperature() {
    return _sensorsData?.userTemperature ?? 0;
  }

  int getUserVentilation() {
    return _sensorsData?.userVentilation ?? 20;
  }

  double getRecuperationPercentage() {
    double t4 = getOutsideAirAfterExchange();
    double t2 = getInsideAirBeforeExchange();
    double t1 = getOutsideAirBeforeExchange();
    if (t4 >= t1) {
      return 99.9;
    } else if (t2 > t1)
      return roundToPrecision((t4 - t1) / (t2 - t1) * 100);
    else
      return 0;
  }

  double roundToPrecision(double value) {
    try {
      String temp = value.toStringAsFixed(1);
      return double.parse(temp);
    } catch (e) {
      return 0;
    }
  }
}
